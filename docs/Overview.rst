.. index documentation master file, created by
   sphinx-quickstart on Thu Mar 14 14:59:47 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Overview
==============================================


=========================
I.Download
=========================

**Link here**

==========================
II.Setup Info
==========================

GametvSdk send for you all information include: gameId, package name, FacebookAppId,payment,list api,... allthing have found in documentation.In DemoProject we attack contain all thing you need.If you don't understand please contact with Techsupport mycompany.

==========================
III.Add Sdk to Project
==========================

1. Add file .aar in folder libs.

.. image:: images/1.import_libs.PNG

2. Select File>> New>> Import Module

.. image:: images/2.import_libs.PNG

3. Select File>> ProjectS Structure
4. Select Module "app".
5. Select tabs "Dependencies" ấn dấu "+" Select "3 Module Dependencies"

.. image:: images/3.import_libs.PNG

6. Select Modules to add

.. image:: images/4.import_libs.PNG

In file build.gradle will add libs GametvSdk auto.

.. image:: images/5.import_libs.PNG


7. Add library into file app/build.grape::

    implementation 'com.google.firebase:firebase-messaging:17.4.0'
    implementation 'com.facebook.android:facebook-android-sdk:[5,6)'
    implementation 'com.google.code.gson:gson:2.6.2'
    implementation 'com.squareup.retrofit2:converter-gson:2.0.2'
    implementation 'com.squareup.okhttp3:logging-interceptor:3.9.0'
    implementation 'com.anjlab.android.iab.v3:library:1.0.44'
    implementation 'com.appsflyer:af-android-sdk:4.8.7@aar'
    implementation 'com.android.installreferrer:installreferrer:1.0'
    implementation project(':gametvsdklib')
    // DeepLink
    implementation 'com.google.firebase:firebase-invites:16.0.3'
    implementation 'com.google.firebase:firebase-core:16.0.3'
    implementation 'com.google.firebase:firebase-dynamic-links:16.0.3'
    
    apply plugin: 'com.google.gms.google-services'

8. Add library into file build.grape::	
    
	dependencies {
        classpath 'com.android.tools.build:gradle:3.3.2'
        classpath 'com.google.gms:google-services:4.2.0'
        // NOTE: Do not place your application dependencies here; they belong
        // in the individual module build.gradle files
    }
	
9. Add line code into file "strings.xml" ::

     <!--FaceBook app id-->
    <string name="facebook_app_id">874432442899415</string>
    <string name="fb_login_protocol_scheme">fb874432442899415</string>
	
You have finish add library to your project.