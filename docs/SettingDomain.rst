.. index documentation master file, created by
   sphinx-quickstart on Thu Mar 14 14:59:47 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Setting Environment 
==============================================

In Your app main activity::
  
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /*Check domain server,true domain submit, false domain test*/
        GTVSDK.getInstance().init(this,[true\false]);
    }