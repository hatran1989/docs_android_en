.. index documentation master file, created by
   sphinx-quickstart on Thu Mar 14 14:59:47 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Payment
==============================================
==========================
I.Payment
==========================
** Use this line to open Payment webview::

	//server_id: game server id
	//role_id: param from game
	//platform: "android"
	//level: character level
	GTVSDK.getInstance().getPaymentMethod(server_id, role_id, platform, level);
	
==================================================
II.IAP Payment(Only for game code name GOGPR02)
==================================================
** Use this line::

    //server_id: server id game
    //role_id: param from game
    //platform: ios or android
    //level: level Gamer   
    //partner:ID Channel payment
    //game_orderid:
    //product_id:
    GTVSDK.getInstance().startIAPPayment(serverID,roleID,platforms,levels,product_id,game_orderid,partner);
	
=============================
III. Check level IAP Payment 
=============================
** Use this line::

    // level: level Gamer 	
    GTVSDK.getInstance().isIAP(level);	