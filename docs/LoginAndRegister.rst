.. index documentation master file, created by
   sphinx-quickstart on Thu Mar 14 14:59:47 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Authentication
==============================================

==========================
I.Login
==========================
		
Use this line to open authentication webview::
     
	GTVSDK.getInstance().onLogin();
	
After login webview closed, call this line to get user_hash::
	
	String user_hash = GTVSDK.getInstance().checkHaveUserhash();

If user_hash is empty the login is unsuccessful

Send this user_hash to your server for validation. Use server api "get_info" to validate it. Check server document.

===============================
II.Logout
===============================

Use this line to logout::

	GTVSDK.getInstance().onLogout();
