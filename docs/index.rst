.. index documentation master file, created by
   sphinx-quickstart on Thu Mar 14 14:59:47 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Setup
==============================================             
			 

.. toctree::
   :maxdepth: 1
   :hidden:
   :titlesonly:

   index
   SettingDomain
   LoginAndRegister
   Payment
   Tracking
   Other
   

=========================
I.Download
=========================

==========================
II. Setup steps
==========================

1. Add file .aar in folder libs.

.. image:: images/1.import_libs.PNG

2. Select File>> New>> Import Module

.. image:: images/2.import_libs.PNG

3. Select File>> ProjectS Structure
4. Select Module "app".
5. Select tabs "Dependencies" press "+" Select "3 Module Dependencies"

.. image:: images/3.import_libs.PNG

6. Select Modules to add

.. image:: images/4.import_libs.PNG

In file build.gradle will add libs GametvSdk auto.

.. image:: images/5.import_libs.PNG


7. Add library into file build.grape::

    implementation 'com.google.firebase:firebase-messaging:17.4.0'
    implementation 'com.facebook.android:facebook-android-sdk:[5,6)'
    implementation 'com.google.code.gson:gson:2.6.2'
    implementation 'com.squareup.retrofit2:converter-gson:2.0.2'
    implementation 'com.squareup.okhttp3:logging-interceptor:3.9.0'
    implementation 'com.anjlab.android.iab.v3:library:1.0.44'
    implementation 'com.appsflyer:af-android-sdk:4.8.7@aar'
    implementation 'com.android.installreferrer:installreferrer:1.0'
    implementation project(':gametvsdklib')
    // DeepLink
    implementation 'com.google.firebase:firebase-invites:16.0.3'
    implementation 'com.google.firebase:firebase-core:16.0.3'
    implementation 'com.google.firebase:firebase-dynamic-links:16.0.3'
	//Adjust
    implementation 'com.adjust.sdk:adjust-android:4.18.3'
    implementation 'com.android.installreferrer:installreferrer:1.0'
    implementation 'com.adjust.sdk:adjust-android-webbridge:4.18.2'
    implementation 'com.google.android.gms:play-services-analytics:16.0.3'
    implementation 'com.android.installreferrer:installreferrer:1.0'
	
	
8. Add line code into file "strings.xml" ::

    <!--FaceBook app id-->
    <string name="facebook_app_id">your app facebook id, example: 123456789012345</string>
    <string name="fb_login_protocol_scheme">your app facebook scheme, example: fb123456789012345</string>
	
9. Add permission into file Android manifest:

** Add permission connect internet ::

	 <uses-permission android:name="android.permission.INTERNET"></uses-permission>     
	 <uses-permission android:name="com.android.vending.BILLING" />

10. Modify Android Manifest

** Add activity into file Android manifest::

        <activity
            android:name="com.vn.admin.gametvsdklib.HomeActivity"
            android:launchMode="singleTask"
            android:screenOrientation="landscape">

            <intent-filter>
                <action android:name="android.intent.action.VIEW" />

                <category android:name="android.intent.category.DEFAULT" />
                <category android:name="android.intent.category.BROWSABLE" />

                <data
                    android:host="deeplinkgtv.page.link"
                    android:scheme="https" />
            </intent-filter>
        </activity>	
		 <service android:name="com.vn.admin.gametvsdklib.notify.MyFirebaseMessagingService">
            <intent-filter>
                <action android:name="com.google.firebase.MESSAGING_EVENT" />
            </intent-filter>
        </service>

        <meta-data
            android:name="com.facebook.sdk.ApplicationId"
            android:value="@string/facebook_app_id" />

        <activity
            android:name="com.facebook.FacebookActivity"
            android:configChanges="keyboard|keyboardHidden|screenLayout|screenSize|orientation"
            android:label="@string/app_name" />
        <activity
            android:name="com.facebook.CustomTabActivity"
            android:exported="true">
            <intent-filter><action android:name="android.intent.action.VIEW" />

                <category android:name="android.intent.category.DEFAULT" />
                <category android:name="android.intent.category.BROWSABLE" />

                <data android:scheme="@string/fb_login_protocol_scheme" />
            </intent-filter>
        </activity>
		  <!--Share faceboook-->
        <provider
            android:name="com.facebook.FacebookContentProvider"
            android:authorities="com.facebook.app.FacebookContentProvider{facebook_app_id}"
            android:exported="true" />

        <!--AppFlyer-->
        <receiver
            android:name="com.appsflyer.SingleInstallBroadcastReceiver"
            android:exported="true">

            <intent-filter>

                <action android:name="com.android.vending.INSTALL_REFERRER" />

            </intent-filter>
        </receiver>
		<activity
            android:name="com.vn.admin.gametvsdklib.InAppPurchaseActivity"
            android:launchMode="singleTask"
            android:screenOrientation="landscape">
        </activity>
		 <!--Adjust-->
        <receiver
            android:name="com.adjust.sdk.AdjustReferrerReceiver"
            android:exported="true" >
            <intent-filter>
                <action android:name="com.android.vending.INSTALL_REFERRER" />
            </intent-filter>
        </receiver>

11. In your main activity add these block code:

** in onCreate add this line::

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        GTVSDK.getInstance().init(this);        

    }

** In onActivityResult add this line:: 

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

         GTVSDK.getInstance().onActivityResult(requestCode, resultCode, data);

    }